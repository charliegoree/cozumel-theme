import App from './app/app.riot'
import {component} from 'riot'
import route from 'riot-route'
route.base('/')
route.start(true)

component(App)(document.querySelector('#root'))