import riot from 'riot'
import observable from 'riot-observable'

function Rutas(){
    observable(this);
    this.rutaActual = "";

    this.on('cambio', function(ruta) {
        this.rutaActual = ruta;
        window.history.replaceState(null,"",ruta);
    });
}

export default new Rutas();